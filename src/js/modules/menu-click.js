export function open (){
    let menuBtn = document.querySelector('.menu');
    let menu = document.querySelector('.up-menu__item');
    return menuBtn.addEventListener('click', function(){
        menuBtn.classList.toggle('active');
        menu.classList.toggle('active');
    })
}
